package com.example.archcompsample

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel : ViewModel() {
    //Utilizando liveData
//    var scoreTeamA = MutableLiveData<Int>().apply { value = 0 }
//    var scoreTeamB = MutableLiveData<Int>().apply { value = 0 }
    var score = Score()

    fun updateTeamA() {
        //update(scoreTeamA)
        score.teamA++
    }

    fun updateTeamB() {
        //update(scoreTeamB)
        score.teamB++
    }

    fun reset() {
        score.teamA = 0
        score.teamB = 0
    }

    //Utilizando o liveData
//    private fun update(score: MutableLiveData<Int>) {
//        val value = score.value ?: 0
//        score.value = value + 1
//    }
}