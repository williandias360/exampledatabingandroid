package com.example.archcompsample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.archcompsample.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.bind<ActivityMainBinding>(viewRoot)!!
    }

    private val viewModel: ScoreViewModel by lazy {
        ViewModelProviders.of(this).get(ScoreViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding.score = viewModel.score
        buttonTeamA.setOnClickListener { viewModel.updateTeamA() }
        buttonTeamB.setOnClickListener { viewModel.updateTeamB() }
        buttonReset.setOnClickListener { viewModel.reset() }


        /*
        Utilizando liveData
        viewModel.scoreTeamA.observe(this, Observer { value ->
            textTeamA.text = value.toString()
        })

        viewModel.scoreTeamA.observe(this, Observer { value ->
            textTeamA.text = value.toString()
        })*/
    }
}
