package com.example.archcompsample

import android.widget.TextView
import androidx.annotation.NonNull
import androidx.databinding.BindingAdapter

class TextViewBinding {
    companion object {
        @BindingAdapter("android:text")
        @JvmStatic
        fun setIntoToText(@NonNull textView: TextView, @NonNull anInt: Int) {
            textView.text = anInt.toString()
        }
    }
}